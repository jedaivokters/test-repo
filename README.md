
# Introduction

![image](acomo-logo.jpg)

This guide is great for easy local set up intended for all developers in the team. It will use docker as a tool on this setup.

## Technical Requirements

 - Install Docker:
	 - Windows 10 Pro, with Enterprise and Education: https://docs.docker.com/docker-for-windows
	 - Windows 7 or Higher: **For legacy system; use docker tool** https://github.com/docker/toolbox/releases
	 - MacOS: [https://docs.docker.com/docker-for-mac/install/](https://docs.docker.com/docker-for-mac/install/)

## Initial Set-up

1. Clone code on your preferred directory. sample below
	 - On Windows: `C:/Users/Username/Documents/Projects/acomo/`
	 - On Mac: `~/Documents/Projects/acomo/`
2. Open terminal. Go to code folder, navigate into `images/php/app/`
	- **note**: *On windows you might need to use 3rd party software like git bash or the docker quick start terminal*
3. On terminal. `docker run --rm -it -v $(pwd):/app composer install`
	- Note: If composer already installed on your system you can simply run `composer install`
4. On terminal: Change directory into root directory folder of the project.
5. On terminal: `docker-compose up --build -d`
6. Test on browser: 
	- Windows 10 Pro / MacOS: [http://localhost](http://localhost)
	- Windows 7 or Higher (legacy using docker tool): [http://192.168.99.100](http://192.168.99.100)

## Basic commands after set up
*On terminal change directory onto the project folder
1. Stop: `docker-compose down`
2. Start:  `docker-compose up`
3. Restart: `docker-compose restart`

-----
### (With Intermediate knowledge on docker) 
To change configuration values, look in the `docker-compose.yml`

## Docker Reference

### [Docker for Mac](https://docs.docker.com/docker-for-mac/)

### [Docker for Windows](https://docs.docker.com/docker-for-windows/)

### [Docker for Linux](https://docs.docker.com/engine/installation/linux/)

## Contribute

Submit a Pull Request!
