<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel', function (Blueprint $table) {
            //
            $table->bigIncrements('hotel_id');
            $table->smallInteger('hotel_group_id');
            $table->char('name', 255);
            $table->char('account', 255); 
            $table->char('password', 255); 
            $table->smallInteger('postal_code');
            $table->char('prefecture', 255);
            $table->char('city', 255);
            $table->char('street', 255);
            $table->smallInteger('phone');
            $table->smallInteger('m_country_code_id');
            $table->char('url', 255);
            $table->smallInteger('status_code');
            $table->smallInteger('delete_flg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hotel');
    }
}
