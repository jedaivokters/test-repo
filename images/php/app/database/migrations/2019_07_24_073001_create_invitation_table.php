<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation', function (Blueprint $table) {
            //
            $table->bigIncrements('invitation_id');
            $table->smallInteger('hotel_id');
            $table->smallInteger('agency_id');
            $table->char('invite_email', 255);
            $table->smallInteger('status_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('invitation');
    }
}
