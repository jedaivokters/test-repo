<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            //
            $table->bigIncrements('room_id');
            $table->smallInteger('hotel_id');
            $table->smallInteger('room_type_id');
            $table->smallInteger('bed_type_id');
            $table->smallInteger('pax_with_bed');
            $table->smallInteger('pax_without_bed');
            $table->smallInteger('smoking_enable_flg');
            $table->smallInteger('size');
            $table->char('name', 255);
            $table->smallInteger('count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room');
    }
}
