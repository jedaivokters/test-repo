<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomEquipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_equipment', function (Blueprint $table) {
            //
            $table->bigIncrements('room_equipment_id');
            $table->smallInteger('room_id');
            $table->smallInteger('equipment_id');
            $table->char('comment', 255);
            
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_equipment');
    }
}
