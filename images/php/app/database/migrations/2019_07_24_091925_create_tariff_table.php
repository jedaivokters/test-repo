<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTariffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariff', function (Blueprint $table) {
            //
            
            $table->bigIncrements('tariff_id');
            $table->smallInteger('hotel_id');
            $table->char('name', 255);
            $table->smallInteger('tariff_number');
            $table->smallInteger('pax_min');
            $table->smallInteger('pax_max');
            $table->date('start_date'); 
            $table->date('end_date');
            $table->smallInteger('tax_inclusion');
            $table->smallInteger('vat_inclusion');
            $table->smallInteger('tax_other');
            $table->time('checkin_time');
            $table->time('checkout_time');
            $table->char('conductor_free_count', 255);
            $table->smallInteger('commission_info');
            $table->char('general_info', 255);
            $table->char('cancel_policy', 255);
            $table->smallInteger('status_code');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tariff');
    }
}
