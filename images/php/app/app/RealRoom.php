<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class RealRoom extends Model {
	protected $table = 'real_room';
	protected $primaryKey = 'real_room_id';

	protected $fillable = [
		"real_room_id",
		"room_id",
		"real_room_code",
		"created_at",
		"updated_at"
	];
}