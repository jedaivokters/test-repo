<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Room extends Model {
	protected $table = 'room';
	protected $primaryKey = 'room_id';

	protected $fillable = [
		"hotel_id",
		"room_type_id",
		"bed_type_id",
		"pax_with_bed",
		"pax_without_bed",
		"smoking_enable_flg",
		"size",
		"name",
		"count",
	];
}