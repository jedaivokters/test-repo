<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class RoomType extends Model {
	protected $table = 'room_type';
	protected $primaryKey = 'room_type_id';

	protected $fillable = [    "room_type_id",
		"hotel_id",
		"name",
		"created_at",
		"updated_at"];
}