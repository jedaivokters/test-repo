<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class VacancyState extends Model {
	protected $table = 'vacancy_state';
	protected $primaryKey = 'vacancy_state_id';

	protected $fillable = [];
}