<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class BedType extends Model {
	protected $table = 'bed_type';
	protected $primaryKey = 'bed_type_id';

	protected $fillable = [
		"bed_type_id",
		"hotel_id",
		"name",
		"created_at",
		"updated_at"
	];
}