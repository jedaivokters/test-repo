<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class RoomEquipment extends Model {
	protected $table = 'room_equipment';
	protected $primaryKey = 'room_equipment_id';

	protected $fillable = [    "room_equipment_id",
		"room_id",
		"equipment_id",
		"comment",
		"created_at",
		"updated_at"];
}