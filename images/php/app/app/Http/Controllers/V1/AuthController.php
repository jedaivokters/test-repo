<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\UserAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;


class AuthController extends Controller {
	/**
	 * @var JWTAuth
	 */
	private $jwtAuth;
	/**
	 * @var Request
	 */
	private $request;
	private $guard;
	/**
	 * @var UserAccess
	 */
	private $userAccess;

	/**
	 * @var \Tymon\JWTAuth\JWTAuth
	 */

	public function __construct(JWTAuth $jwtAuth, UserAccess $userAccess) {

		$this->middleware('auth:api', ['except' => ['login']]);
		$this->jwtAuth = $jwtAuth;


		$this->userAccess = $userAccess;
	}



	public function login(Request $request) {
		$this->validate($request, [
			'email'    => 'required|max:255',
			'password' => 'required',
		]);

		$credential = $request->only('email', 'password');
		$token = $this->jwtAuth->attempt($credential);
		if (!$token) {
			$error = [

				"user" => ["user not found"],
			];


			$response = [
				"code"    => 404,
				'status'  => 'error',
				'data'    => ['form_validations' => $error, 'exception' => 'NotFoundException'],
				'message' => 'Not Found',
			];


			return response()->json($response, 404);
		}

		return $this->respondWithToken($token);

	}

	public function profile() {

		$inputs = [
			'status'  => true,
			'message' => 'success',
			'code'    => 200,
			'data'    => $this->jwtAuth->parseToken()->toUser(),
		];


		return response()->json($inputs, 200);
	}

	public function logout() {
		$this->jwtAuth->parseToken()->invalidate();
		$inputs = [
			'status'  => true,
			'message' => 'Successfully logged out',
			'code'    => 200,
			'data'    => [],
		];

		return response()->json($inputs, 200);
	}

	protected function respondWithToken($token) {

		$user = $this->jwtAuth->user();
		$access =  $this->userAccess->whereUserId($user->id)->first();

		$inputs = [
			'status'  => true,
			'message' => 'success',
			'code'    => 200,
			'data'    => [
				'access_token' => $token,
				'user_id' => $user->id,
				'type' => $access->type
			],
		];

		return response()->json($inputs, 200);
	}

}