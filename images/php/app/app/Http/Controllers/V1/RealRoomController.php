<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;


class RealRoomController extends Controller {
	use RestTrait;
	const MODEL = 'App\RealRoom';
	protected $validationRules = [
		"room_id",
		"real_room_code",

	];


}