<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;


class RoomController extends Controller {
	use RestTrait;
	const MODEL = 'App\Room';
	protected $validationRules = [
		"hotel_id",
		"room_type_id",
		"bed_type_id",
		"pax_with_bed",
		"pax_without_bed",
		"smoking_enable_flg",
		"size",
		"name",
		"count",
	];

//"hotel_id","room_type_id","bed_type_id","pax_with_bed","pax_without_bed","smoking_enable_flg","size","name","count"
}