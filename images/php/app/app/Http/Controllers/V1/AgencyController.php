<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;


class AgencyController extends Controller {
	use RestTrait;
	const MODEL = 'App\Agency';
	protected $validationRules = ["account" => "required", "password"=>"required" , "agency_group_id" => "required"];


}