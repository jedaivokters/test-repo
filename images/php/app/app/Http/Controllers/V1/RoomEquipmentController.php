<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;


class RoomEquipmentController extends Controller {
	use RestTrait;
	const MODEL = 'App\RoomEquipment';
	protected $validationRules = [
		"room_id",
		"equipment_id",
		"comment",

	];


}