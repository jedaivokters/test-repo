<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;
use App\User;
use Illuminate\Http\Request;


class UserController extends Controller {
	use RestTrait;
	const MODEL = 'App\User';
	protected $validationRules = ["email" => 'required|email|unique:users', "password" => "required"];
	/**
	 * @var User
	 */
	private $user;


	public function __construct(User $user) {

		$this->user = $user;
	}

	/**
	 * @param Request $request
	 */
	public function store(Request $request) {

		$data = $request->all();


		try {
			$v = \Validator::make($data, $this->validationRules);

			if ($v->fails()) {
				throw new \Exception("ValidationException");
			}
			$response = $this->user->create($data);


			$access = [
				"platform_id" => $data['platform_id'],
				"user_id"     => $response->id,
				"type"        => $data['type'],
				"role"        => "admin",
			];
			$response->hasManyUserAccess()->create($access);

			$response = [
				'code'   => 201,
				'status' => 'succcess',
				'data'   => $response,
			];

			return response()->json($response, 200);


		} catch (\Exception $ex) {

			$error = ['form_validations' => $v->errors(), 'exception' => $ex->getMessage()];

			$response = [
				'code'    => 422,
				'status'  => 'error',
				'data'    => $error,
				'message' => 'Unprocessable entity',
			];

			return response()->json($response, $response['code']);

		}


	}

	public function requestForAccount(Request $request) {
		$data = $request->all();


		try {
			$v = \Validator::make($data, [
				"email" => "required|email|unique:users",
			]);

			if ($v->fails()) {
				throw new \Exception("ValidationException");
			}


			return $this->createdResponse($data);
		} catch (\Exception $ex) {
			$response = ['form_validations' => $v->errors(), 'exception' => $ex->getMessage()];

			return $this->clientErrorResponse($response);
		}

	}

}