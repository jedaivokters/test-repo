<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Traits\RestTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class HotelController extends Controller {
	use RestTrait;
	const MODEL = 'App\Hotel';
	protected $validationRules = ["account" => "required", "password" => "required", "hotel_group_id" => "required"];
	/**
	 * @var ResponseHelper
	 */
	private $responseHelper;

//	public function __construct(ResponseHelper $responseHelper) {
//
//		$this->responseHelper = $responseHelper;
//	}



}