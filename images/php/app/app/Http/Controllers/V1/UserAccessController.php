<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Traits\RestTrait;


class UserAccessController extends Controller {
	use RestTrait;
	const MODEL = 'App\UserAccess';
	protected $validationRules = ["platform_id" => "required","type" => "required","user_id" => "required"];


}