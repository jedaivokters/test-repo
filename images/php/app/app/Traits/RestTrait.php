<?php

namespace App\Traits;
use Illuminate\Http\Request;

trait RestTrait
{

	/**
	 * @var Request
	 */
	private $request;

	public function __construct(Request $request){

		$this->request = $request;
	}
	public function index()
	{
		$m = self::MODEL;
		return $this->listResponse($m::all());
	}

	public function show($id)
	{
		$m = self::MODEL;
		if($data = $m::find($id))
		{
			return $this->showResponse($data);
		}
		return $this->notFoundResponse();
	}

	public function store()
	{


		$data= $this->request->all();

		$m = self::MODEL;


		try
		{
			$v = \Validator::make($data, $this->validationRules);

			if($v->fails())
			{
				throw new \Exception("ValidationException");
			}
			$response = $m::create($data);
			return $this->createdResponse($response);
		}catch(\Exception $ex)
		{
			$response = ['form_validations' => $v->errors(), 'exception' => $ex->getMessage()];
			return $this->clientErrorResponse($response);
		}

	}

	public function update($id)
	{


		$data= $this->request->all();

		$m = self::MODEL;

		if(!$response = $m::find($id))
		{
			return $this->notFoundResponse();
		}

		try
		{
			$v = \Validator::make($data, $this->validationRules);

			if($v->fails())
			{
				throw new \Exception("ValidationException");
			}
			$response->fill($data);
			$response->save();
			return $this->showResponse($response);
		}catch(\Exception $ex)
		{
			$response = ['form_validations' => $v->errors(), 'exception' => $ex->getMessage()];
			return $this->clientErrorResponse($response);
		}
	}

	public function destroy($id)
	{
		$m = self::MODEL;
		if(!$data = $m::find($id))
		{
			return $this->notFoundResponse();
		}
		$data->delete();
		return $this->deletedResponse();
	}

	protected function createdResponse($data)
	{
		$response = [
			'code' => 201,
			'status' => 'succcess',
			'data' => $data
		];
		return response()->json($response, $response['code']);
	}

	protected function showResponse($data)
	{
		$response = [
			'code' => 200,
			'status' => 'succcess',
			'data' => $data
		];
		return response()->json($response, $response['code']);
	}

	protected function listResponse($data)
	{
		$response = [
			'code' => 200,
			'status' => 'succcess',
			'data' => $data
		];
		return response()->json($response, $response['code']);
	}

	protected function notFoundResponse()
	{
		$response = [
			'code' => 404,
			'status' => 'error',
			'data' => 'Resource Not Found',
			'message' => 'Not Found'
		];
		return response()->json($response, $response['code']);
	}

	protected function deletedResponse()
	{
		$response = [
			'code' => 204,
			'status' => 'success',
			'data' => [],
			'message' => 'Resource deleted'
		];
		return response()->json($response, $response['code']);
	}

	protected function clientErrorResponse($data)
	{
		$response = [
			'code' => 422,
			'status' => 'error',
			'data' => $data,
			'message' => 'Unprocessable entity'
		];
		return response()->json($response, $response['code']);
	}

}