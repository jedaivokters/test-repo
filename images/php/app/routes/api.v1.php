<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

function rest($router, $path, $controller) {


	$router->get($path, $controller . '@index');
	$router->get($path . '/{id}', $controller . '@show');
	$router->post($path, $controller . '@store');
	$router->put($path . '/{id}', $controller . '@update');
	$router->delete($path . '/{id}', $controller . '@destroy');
}


rest($router, '/hotels', 'HotelController');
rest($router, '/agencies', 'AgencyController');
rest($router, '/rooms', 'RoomController');
rest($router, '/users', 'UserController');
rest($router, '/user-accesses', 'UserAccessController');
rest($router, '/bed-types', 'BedTypeController');

rest($router, '/real-rooms', 'RealRoomController');
rest($router, '/room-equipments', 'RoomEquipmentController');
rest($router, '/room-types', 'RoomTypeController');
rest($router, '/vacancy-types', 'VacancyTypeController');
rest($router, '/vacancy-states', 'VacancyStateController');

$router->post('users/request-for-account', 'UserController@requestForAccount');

$router->group(['prefix' => "auth"], function ($router) {

	$router->post('/login', 'AuthController@login');
	$router->post('/logout', 'AuthController@logout');
	$router->get('/profile', 'AuthController@profile');


});